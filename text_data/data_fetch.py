import numpy as np
import operator
import sys
import math

'''
fname: name of file
n:     length of ngrams to be returned
            --> "1" returns 1-grams, etc.
'''
def fetch(fname, n = 1):
    # parse file
    with open(fname, 'r') as f:
        content = [x.strip().lower() for x in f.readlines() if len(x.strip()) > 0]
        content = ' ||P|| '.join(content)
        words = content.split()
    # word counts
    counts = {}
    for word in words:
        if word not in counts:
            counts[word] = 0
        counts[word] += 1
    # get ordered data
    pairs = list(reversed(sorted(counts.items(), key=operator.itemgetter(1))))
    for ndx in range(len(pairs)):
        counts[pairs[ndx][0]] = ndx
    # build numeric dataset
    data = [counts[word] for word in words]
    
    # make windows
    target = []
    windowed = []
    for i in range(len(data) - n):
        ele = [data[i]]
        for delta in range(1, n):
            ele.append(data[i + delta])
        windowed.append(ele)
        target.append(data[i + n])
    # translator
    translate = {}
    for w in counts:
        translate[counts[w]] = w
    return windowed, target, translate

'''
cleans a text file, outputs "cleaned-[fname]"
'''
def clean(fname):
    with open(fname, 'r') as f:
        lines = f.readlines()
    for i in range(len(lines)):
        for p in '.,?!():;$#@%^&*-_+=[]{}"/\|<>' + '"':
            lines[i] = lines[i].replace(p, ' ' + p)
            lines[i] = lines[i].lower()
    with open('cleaned-' + fname, 'w+') as f:
        for line in lines:
            f.write(line)

'''
Returns the string version of the word number
'''
def neighbors(num, translate):
    if type(num) == int or math.floor(num) == math.ceil(num):
        return translate[int(num)]
    return '"' + translate[math.floor(num)] + '", "' + translate[math.ceil(num)] + '"'

if __name__ == '__main__':
    if '-c' in sys.argv:
        clean('test.txt')
    else:
        data, tgt, c = fetch('fable.txt', 3)
        print(data)
        print()
        print(tgt)
        
        
        
        
        
