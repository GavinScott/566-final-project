import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from lstm import LSTM
from common import _assert_all_finite, _drop_nans_idx
from sklearn.decomposition import PCA
from sklearn.metrics import mean_squared_error, accuracy_score

load_array = lambda path: np.load(path)
sharpe_ratio = lambda return_curve: float(np.mean(return_curve) / np.std(return_curve, ddof=0))
sortino_ratio = lambda return_curve: float(np.mean(return_curve) / np.std(return_curve[return_curve < 0], ddof=0))

def main():
    np.random.seed(0)
    print('*****************************************************************')
    pfx = './S&P500/'
    fm = process_and_save(pfx)
    print('done\n')
    print('*****************************************************************')

    # load labels into memory
    print('load labels...', end='')
    all_dates = load_array(pfx + './lab/' + 'SPX-dates.npy')
    net_return = load_array(pfx + './lab/' + 'SPX-net_return.npy')
    lb_price_dir = load_array(pfx + './lab/' + 'SPX-lb_price_dir.npy')
    print('done\n')
    print('*****************************************************************')

    # Classification preparation
    print('Classification Readout')
    train_bound = int(.8 * fm.shape[0])
    to_drop = _drop_nans_idx(lb_price_dir)
    data = np.delete(np.copy(fm), to_drop, axis=0)
    label = np.delete(np.copy(lb_price_dir), to_drop, axis=0)
    dates = np.delete(np.copy(all_dates), to_drop, axis=0)
    pct = np.delete(np.copy(net_return), to_drop, axis=0)
    _assert_all_finite(data)
    _assert_all_finite(label)
    _assert_all_finite(pct)
    #label[label == -1] = 0
    label = label.astype(np.int64)
    
    num_comp = 10
    print('pca with', num_comp, 'components...', end='')
    pca = PCA(n_components=num_comp)
    data = np.matrix(pca.fit_transform(data))
    print('\ndata shape:', data.shape)
    print('label shape:', label.shape)
    print('done\n')
    
    # Classification
    print('LSTM:')
    lstm = LSTM(layers=[data.shape[1], 1], learning_rate=.00000001, verbose=True)
    lstm.fit(data[:train_bound], label[:train_bound], max_iter=10)
    lstm_guesses = lstm.predict(data[train_bound:], label.shape)
    acc = accuracy_score(label[train_bound:], lstm_guesses[train_bound:])
    print('accuracy: {0:.8f}'.format(acc))
    #print('prior distribution of [down, up]:', np.bincount(label[train_bound:]) / label[train_bound:].shape[0])
    print()

    # Classification equity curve graphing
    test_period = pd.to_datetime(dates, format='%Y-%m-%d').values[train_bound:]
    long_only, short_only, long_short, long_hold = equity_curves(lstm_guesses, label[train_bound:], pct[train_bound:])
    plt.plot(test_period, long_only[1:], color='green', label='long only')
    plt.plot(test_period, short_only[1:], color='red', label='short only')
    plt.plot(test_period, long_short[1:], color='blue', label='long short')
    plt.plot(test_period, long_hold[1:], color='black', label='long hold (benchmark)')
    plt.xlabel('Time (hour)')
    plt.ylabel('Cumulative Return')
    title = 'S&P 500 Logistic Classification Equity Curves from ' +str(test_period[0])[:10] +' to ' +str(test_period[-1])[:10]
    plt.title(title)
    plt.legend()
    plt.show()

    print('long only final value: {0:1.4f}'.format(long_only[-1]))
    print('short only final value: {0:1.4f}'.format(short_only[-1]))
    print('long short final value: {0:1.4f}'.format(long_short[-1]))
    print('long hold (benchmark) final value {0:1.4f}:'.format(long_hold[-1]))
    print()

    print('long only sharpe ratio: {0:1.4f}'.format(sharpe_ratio(np.diff(long_only))))
    print('short only sharpe ratio: {0:1.4f}'.format(sharpe_ratio(np.diff(short_only))))
    print('long short sharpe ratio: {0:1.4f}'.format(sharpe_ratio(np.diff(long_short))))
    print('long hold (benchmark) sharpe ratio {0:1.4f}:'.format(sharpe_ratio(np.diff(long_hold))))
    print()

    print('long only sortino ratio: {0:1.4f}'.format(sortino_ratio(np.diff(long_only))))
    print('short only sortino ratio: {0:1.4f}'.format(sortino_ratio(np.diff(short_only))))
    print('long short sortino ratio: {0:1.4f}'.format(sortino_ratio(np.diff(long_short))))
    print('long hold (benchmark) sortino ratio: {0:1.4f}'.format(sortino_ratio(np.diff(long_hold))))
    print('*****************************************************************')

def process_and_save(pfx):
    indexers = ['id', 'date']
    targets = ['net_return', 'log_return']
    labels = ['price_dir', 'long_bias_price_dir']

    # Get the data and prepare targets/labels
    data = pd.read_csv(pfx + './feat/' +'SPX-feat.csv') # S&P 500 based features
    dates = data['date'].as_matrix()
    net_return = data['net_return'].as_matrix()
    lb_price_dir = data['long_bias_price_dir'].as_matrix()

    # Save label arrays to file
    np.save(pfx +'./lab/' + 'SPX-dates.npy', dates)
    np.save(pfx +'./lab/' + 'SPX-net_return.npy', net_return)
    np.save(pfx +'./lab/' + 'SPX-lb_price_dir.npy', lb_price_dir)

    # Prepare features
    to_drop = indexers + targets + labels
    feat_matrix = data.drop(to_drop, axis=1).as_matrix()

    return feat_matrix

def equity_curves(predictions, actual, pct):
    """
    :param predictions: ndarray of predicted price direction
    :param actual: ndarray of actual price direction
    :param pct: return statistic used
    """
    correct = predictions == actual
    long_only_curve = np.zeros(actual.shape[0] + 1)
    short_only_curve = np.zeros(actual.shape[0] + 1)
    long_short_curve = np.zeros(actual.shape[0] + 1)
    long_hold_curve = np.zeros(actual.shape[0] + 1)
    long_only = 1
    short_only = 1
    long_short = 1
    long_hold = 1
    long_only_curve[0] = long_only
    short_only_curve[0] = short_only
    long_short_curve[0] = long_short
    long_hold_curve[0] = long_hold

    for i in range(1, actual.shape[0] + 1):
        ret = abs(pct[i-1])
        long_prediction = predictions[i-1] == 1
        long_hold += pct[i-1]
        if (actual[i-1]):
            if (long_prediction):
                long_only += ret
            else:
                short_only += ret
            long_short += ret
        else:
            if (long_prediction):
                long_only -= ret
            else:
                short_only -= ret
            long_short -= ret
        long_only_curve[i] = long_only
        short_only_curve[i] = short_only
        long_short_curve[i] = long_short
        long_hold_curve[i] = long_hold

    return long_only_curve, short_only_curve, long_short_curve, long_hold_curve

if __name__ == "__main__":
    main()
