# Kevin Patel, Gavin Scott
# CSC 566 - Final Project

import copy
import numpy as np
import matplotlib.pyplot as plt
from text_data import data_fetch as df

def main():
    np.seterr(all='raise')  # Have numpy exit execution on any errors (such as underflow)

    # Generate train and test data
    train_inputs = np.random.uniform(low=0, high=20, size=500)
    shift = lambda li: np.array([np.array([li[i-1],li[i],li[i+1]]) for i in range(1, len(li) - 1)])
    train_inputs = shift(train_inputs)
    train_outputs = np.mean(-train_inputs, axis=1) * np.exp(np.mean(-train_inputs, axis=1))
    test_inputs = np.linspace(0, 5, num=50)
    test_inputs = shift(test_inputs)
    test_outputs = np.mean(-test_inputs, axis=1) * np.exp(np.mean(-test_inputs, axis=1))
    
    lstm = LSTM(verbose=True, learning_rate=0.1)
    untrained_predictions = lstm.predict(test_inputs, test_outputs.shape)
    lstm.fit(train_inputs, train_outputs, max_iter=100)
    trained_predictions = lstm.predict(test_inputs, test_outputs.shape)

    plt.scatter(test_inputs[:, 0], untrained_predictions, label='Untrained $f(x)=xe^{-x}$')
    plt.scatter(test_inputs[:, 0], test_outputs, label='Actual $f(x)=xe^{-x}$')
    plt.scatter(test_inputs[:, 0], trained_predictions, label='NN Predicted $f(x)=xe^{-x}$')
    plt.title('Actual and NN Function Approximation')
    plt.legend()
    plt.show()
    
    return 0

sigmoid = lambda x: 1 / (1 + np.exp(-x))
sigmoid_deriv = lambda x: sigmoid(x) * (1 - sigmoid(x))
tanh_deriv = lambda y: 1 - np.power(np.tanh(y), 2)

# Class defines a densely connected feedforward neural network
class LSTM():
    
    # Constructor initializes parameters
    # Layers in the format [input vector size, [hidden layer size...], output layer size]
    def __init__(self, layers=[3, 1], learning_rate=.6, verbose=False):
        self.layers = layers
        self.learning_rate = learning_rate
        self.verbose = verbose
        self.activation = np.tanh
        self.activation_deriv = tanh_deriv
        self.gate_activation = sigmoid
        self.gate_activation_deriv = sigmoid_deriv
        self.output_activation = np.tanh
        self.output_activation_deriv = tanh_deriv

        self.gate_input_fn = lambda weights, n: np.matrix(np.diagonal(weights[n] * self.dataflow[n])).T
        self.gate_layer_fn = lambda: [np.matrix(np.zeros((layers[size+1], layers[size] + 2))) for size in range(len(layers) - 1)]

        # Weights for the LSTM gates
        self.output_weights = self.gate_layer_fn()
        self.forget_weights = self.gate_layer_fn()
        self.write_weights  = self.gate_layer_fn()

        # There are 'len(layers) - 1' weight matrices because the first number in the 'layers' defines
        # the network input size. For each layer, the row indexes the output neuron and the column indexes
        # the input neuron, so matrix[j, i] stores the weight from input neuron i to output neuron j.
        # The second '+ 1' in the list comprehension is to account for the bias input, the zeroth column
        # will be the weights from bias to the next layer. Weights are initialized randomly.
        self.input_weights = [np.matrix(np.random.rand(layers[i+1], layers[i] + 1)) for i in range(len(layers)-1)]

        # Stores the previous states of each node and their weights
        self.dataflow = [np.matrix(np.zeros((layers[size] + 2, layers[size+1]))) for size in range(len(layers) - 1)]
        for i in range(len(self.dataflow)): # Add constant bias input
            self.dataflow[i][0] = np.ones(self.dataflow[i].shape[1])

        # Stores the dataflow for the previous time step (used to store state from t-1 time step)
        self.prev_dataflow = copy.deepcopy(self.dataflow)


    # numpy elementwise multiply with 2D broadcasting
    def _mult(self, a, b):
        if (a.shape[0] < b.shape[0]):
            a = np.matrix(np.repeat(a, b.shape[0], axis=0))
        if (a.shape[1] < b.shape[1]):
            a = np.matrix(np.repeat(a, b.shape[1], axis=1))
        if (b.shape[0] < a.shape[0]):
            b = np.matrix(np.repeat(b, a.shape[0], axis=0))
        if (b.shape[1] < a.shape[1]):
            b = np.matrix(np.repeat(b, a.shape[1], axis=1))
        return np.multiply(a, b)

    # Computes the output of the neural network given an input vector
    def forward_pass(self, input_vec):
        #assert(len(input_vec) == self.layers[0])
        if len(input_vec) != self.layers[0]:
            input_vec = input_vec.T
            #print('INPUT VEC:', np.matrix(input_vec).shape, len(input_vec), self.layers[0])
        input_col_vec = np.reshape(np.matrix(input_vec), (len(input_vec), 1)) # reshape into column vector
        for i in range(len(self.dataflow)):
            self.dataflow[i][1:-1] = np.matrix(np.repeat(input_col_vec, self.dataflow[i].shape[1], axis=1))
            np.copyto(self.prev_dataflow[i], self.dataflow[i])
        
        # Loop performs cascaded linear combinations across weight matrices
        for i in range(len(self.layers) - 1):
            assert(self.input_weights[i].shape[1] == self.dataflow[i].shape[0]-1)

            # Output of this layer becomes input to the next
            if (i == len(self.layers) - 2): # Output layer
                #print('\noutput reached ({})'.format(i))
                act = self.output_activation
            else:
                #print('\nhidden layer ({})'.format(i))
                act = self.activation

            output = act(self.input_weights[i] * self.dataflow[i][:-1, 0])
            
            # Calculate gate outputs
            # print('input:', self.write_weights[i].shape)
            # print('diag:', np.matrix(np.diagonal(self.write_weights[i] * self.dataflow[i])).shape)
            input_gate = self.gate_activation(self.gate_input_fn(self.write_weights, i))
            output_gate = self.gate_activation(self.gate_input_fn(self.output_weights, i))
            forget_gate = self.gate_activation(self.gate_input_fn(self.forget_weights, i))
            # print('gate:', input_gate.shape)
            # print('output:', output.shape)
            
            prev_state = np.multiply(output, input_gate) + np.multiply(forget_gate, self.dataflow[i][-1].T)
            output = np.multiply(output_gate, prev_state)

            self.dataflow[i][-1] = prev_state.T
            if (i < len(self.layers) - 2): # Hidden layer
                self.dataflow[i+1][1:-1] = output # Indexed to not overwrite bias input or previous state

        return output

    # Runs backpropagation and returns a list of weight delta matrices sorted by layer
    # Objective Function used is E=.5*e^2, where e=d-y
    def backward_pass(self, expected, predicted):
        deltas = []

        # common
        # E = .5 (e**2)
        error = - (expected - predicted)  #dE / dy
        gate_input = self.dataflow[-1].T
        previous_state = self.prev_dataflow[-1][-1].T

        # output gate
        output_gate_input = self.gate_input_fn(self.output_weights, -1)
        # print('\noutput_gate_input.shape:      ', output_gate_input.shape)
        # print('self.dataflow[-1][-1].T.shape:', self.dataflow[-1][-1].T.shape)
        # print('gate_input.shape:             ', gate_input.shape)
        # gate_act_dataflow = np.multiply(self.gate_activation_deriv(output_gate_input), self.dataflow[-1][-1].T)
        # if (gate_act_dataflow.shape[0] != gate_act_dataflow.shape[0]):
        #     gate_act_dataflow = np.repeat(gate_act_dataflow, gate_act_dataflow.shape[0])
        '''
        print('\n0:', self.output_weights)
        print('1:', error)
        print('2:', output_gate_input)
        print('3:', self.gate_activation_deriv(output_gate_input))
        print('4:', self.dataflow[-1][-1].T)
        print('5:', gate_input)
        #exit()
        '''
        output_grad = error * self._mult(self._mult(self.gate_activation_deriv(output_gate_input), previous_state), gate_input)

        # forget gate
        forget_gate_input = self.gate_input_fn(self.forget_weights, -1)
        # print('gate input', gate_input)
        # print(self.prev_dataflow[-1].shape)
        # print(forget_gate_input.shape)
        # print(output_gate_input.shape)
        forget_grad = error * self._mult(self._mult(self.prev_dataflow[-1], self.gate_activation(output_gate_input)).T, \
            self._mult(self.gate_activation_deriv(forget_gate_input), gate_input))

        # write gate
        write_gate_input = self.gate_input_fn(self.write_weights, -1)
        input_vector = self.dataflow[-1][:-1, 0]
        input_input = self.input_weights[-1] * input_vector
        write_grad = error * self._mult(self._mult(self.gate_activation(output_gate_input), self.output_activation(input_input)), \
            self._mult(self.gate_activation_deriv(write_gate_input), gate_input))

        # input neuron
        input_grad = error * self._mult(self._mult(self.gate_activation(output_gate_input), self.gate_activation(write_gate_input)),
            self._mult(self.output_activation_deriv(input_input), input_vector).T)
        #print('input:grad:', input_grad)
        deltas.append((output_grad, forget_grad, write_grad, input_grad))

        # Compute weight delta matrices for hidden layers:
        # for i in reversed(range(len(self.input_weights) - 1)):
            # output gate
            #output_gate_input = self.gate_input_fn(self.output_weights, i)

            # # Bias of the output layer does not contribute to the weights of this layer, so drop it:
            # out_layer = self.input_weights[i+1][:, 1:].transpose() * product
            # in_layer = self.activation_deriv(self.input_weights[i] * self.dataflow[i])
            # product = np.multiply(out_layer, in_layer) # Hadamard product
            # weight_delta = product * self.dataflow[i].transpose()

            # assert(weight_delta.shape == self.input_weights[i].shape) # assert matches expected
            # deltas.append((output_grad, forget_grad, write_grad, input_grad))

        return list(reversed(deltas)) # Return ascending list of weight delta matrices

    # Updates the weights according to the weight delta matrices passed in
    def update_weights_online(self, weight_deltas, learning_rate):
        for i in range(len(self.input_weights)):
            print('\nWEIGHT DELTA:', weight_deltas[i][:-1])
            self.output_weights[i][:] += np.multiply(weight_deltas[i][0], -learning_rate)
            self.forget_weights[i][:] += np.multiply(weight_deltas[i][1], -learning_rate)
            self.write_weights[i][:] += np.multiply(weight_deltas[i][2], -learning_rate)
            self.input_weights[i][:] += np.multiply(weight_deltas[i][3], -learning_rate)

    # Changes the network weights using the passed training example
    def train_online(self, feature_vec, expected):
        assert(feature_vec.shape[0] == self.layers[0])
        prediction = self.forward_pass(feature_vec)
        weight_deltas = self.backward_pass(expected, prediction)
        self.update_weights_online(weight_deltas, self.learning_rate)

    # Fits the neural net to the training set using either online or batch training
    def fit_online(self, features, labels, num_epochs=10):
        assert(features.shape[0] == labels.shape[0])
        numdigits = len(str(num_epochs))

        for epoch in range(1, num_epochs+1):
            #print('epoch ' + str(epoch).zfill(numdigits), end='')
            for i in range(features.shape[0]):
                self.train_online(features[i], labels[i])
            #print('...done')

    # Updates the weights according to the weight delta matrices passed in
    def update_weights(self, output_deltas, forget_deltas, write_deltas, input_deltas, learning_rate, show=False):
        for i in range(len(self.input_weights)):
            if (show and self.verbose):
                print('\nLAYER', str(i) +':')
                print('OUTPUT WEIGHT DELTA:', output_deltas[i])
                print('FORGET WEIGHT DELTA:', forget_deltas[i])
                print('WRITE WEIGHT DELTA:', write_deltas[i])
                print('INPUT WEIGHT DELTA:', input_deltas[i])
            self.output_weights[i][:] += np.multiply(output_deltas[i], -learning_rate)
            self.forget_weights[i][:] += np.multiply(forget_deltas[i], -learning_rate)
            self.write_weights[i][:] += np.multiply(write_deltas[i], -learning_rate)
            self.input_weights[i][:] += np.multiply(input_deltas[i], -learning_rate)

    # Train the network on the minibatch of examples, update weights accordingly, and return sse
    def train(self, feats, labs):
        mb_size = feats.shape[0]
        errors = np.zeros(mb_size)

        # Init weight delta accumulators
        mb_output_deltas = self.gate_layer_fn()
        mb_forget_deltas = self.gate_layer_fn()
        mb_write_deltas = self.gate_layer_fn()
        mb_input_deltas = [np.matrix(np.zeros((self.layers[i+1], self.layers[i] + 1))) for i in range(len(self.layers)-1)]
        for i in range(mb_size):
            out = self.forward_pass(feats[i])
            errors[i] = labs[i] - out
            this_delta = self.backward_pass(labs[i], out)

            # Accumulate minibatch weight deltas
            for lay in range(len(self.input_weights)):
                mb_output_deltas[lay][:] += this_delta[lay][0]
                mb_forget_deltas[lay][:] += this_delta[lay][1]
                mb_write_deltas[lay][:] += this_delta[lay][2]
                mb_input_deltas[lay][:] += this_delta[lay][3]

        # Average minibatch weight deltas
        for lay in range(len(self.input_weights)):
            mb_output_deltas[lay][:] = mb_output_deltas[lay] / mb_size
            mb_forget_deltas[lay][:] = mb_forget_deltas[lay] / mb_size
            mb_write_deltas[lay][:] = mb_write_deltas[lay] / mb_size
            mb_input_deltas[lay][:] = mb_input_deltas[lay] / mb_size
        self.update_weights(mb_output_deltas, mb_forget_deltas, mb_write_deltas, mb_input_deltas, self.learning_rate)
        return np.sum(errors**2, axis=0)

    # Fits the neural net to the training set, returns a tuple of (epoch, final sse)
    def fit(self, features, labels, mb_size=1, batch_frac=1, epsilon=.01, epsilon_delta=.0001,
            randomize=False, max_iter=100, show=True):
        '''
        :param mb_size: minibatch size, 1 <= mb_size <= features.shape[0] (online <= mini batch <= batch)
        :param batch_frac: fraction of batch to go through in each epoch
        :param epsilon: global error
        :param epsilon: change in global error
        :param randomize: booelan to shuffle dataset before each epoch
        :param max_iter: number of epochs
        '''
        num_rows = int(batch_frac * features.shape[0])
        assert(features.shape[0] == labels.shape[0])
        assert(batch_frac > 0  and batch_frac <= 1)
        assert(mb_size > 0  and mb_size <= num_rows)
        if (show and self.verbose):
            numdigits = len(str(max_iter))
        num_updates = (num_rows // mb_size) + (1 if num_rows % mb_size > 0 else 0)
        mb_sse = np.zeros(num_updates)
        this_sse = 0
        feats = np.copy(features)[:num_rows]
        labs = np.copy(labels)[:num_rows]

        for epoch in range(max_iter):            
            if (show and self.verbose):
                print('epoch ' + str(epoch).zfill(numdigits), end='')
            if (randomize):
                order = np.random.permutation(features.shape[0])
                feats[:] = features[order][:num_rows]
                labs[:] = labels[order][:num_rows]

            for i in range(num_updates):
                start = i * mb_size
                end = start + mb_size if start + mb_size <= num_rows else num_rows
                mb_sse[i] = self.train(feats[start:end], labs[start:end])
            prev_sse = this_sse
            this_sse = .5 * np.sum(mb_sse, axis=0) # Total sum squared error
            sse_delta = abs(this_sse - prev_sse)
            mb_sse.fill(0)

            if (show and self.verbose):
                print('...done, sse: {0:2.3f}'.format(this_sse))
            if (this_sse <= epsilon and sse_delta <= epsilon_delta):
                if (show and self.verbose):
                    print('\n early convergence')
                    print(' sse: {0:2.3f}'.format(this_sse))
                    print(' delta sse: {0:2.3f}'.format(sse_delta))
                return (epoch, this_sse)
        return (max_iter, this_sse)

    # Passes the feature list through the network and returns a list of predictions
    def predict(self, features, labelshape):
        predictions = np.zeros(labelshape)

        for i in range(features.shape[0]):
            predictions[i] = self.forward_pass(features[i])

        return predictions

if __name__ == "__main__":
    main()
